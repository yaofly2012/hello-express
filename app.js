const express = require('express');
const app = express();
const port = 3000;

// app setting
app.disable('x-powered-by');

app.use('/birds', require('./src/route/birds'))

// 一旦上面的middleware终止了req-res cycle就不会仅需匹配了
app.use(require('./src/middleware/logger'))

app.listen(port, () => {
    console.log(`App listening on port ${port}`)
})